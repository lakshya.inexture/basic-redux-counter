import { combineReducers } from "redux";
import count from "./reducers/userReducer";

const rootReducer = combineReducers({ count });

export default rootReducer;
