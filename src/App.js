import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment } from "./redux/actions/userAction";

function App() {
    const dispatch = useDispatch();
    const { count } = useSelector((state) => state);
    return (
        <div>
            <button onClick={() => dispatch(increment(count))}>
                Increment
            </button>
            <button onClick={() => dispatch(decrement(count))}>
                Decrement
            </button>
            <h1>{count}</h1>
        </div>
    );
}

export default App;
